# README #

A "getting started" project for CIS 322, introduction to software engineering, at the University of Oregon.

Modifications included modifying respond function to show in the browser files found in pages directory as well as transmitting 200 status code. If file not found in directory transmit 404 status code, and finally if file begins with (~, //, ..) transmit status code 403.

Tested with provided tests and with personal tests in browser all seems to work correctly.
## Author: Jaime Antolin Merino, jaimea@uoregon.edu ##
